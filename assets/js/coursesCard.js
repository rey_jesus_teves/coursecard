let token = localStorage.getItem('token');

let courseContainer = document.querySelector('#courseContainer');


	fetch('https://rocky-harbor-60941.herokuapp.com/api/courses',{
		method: 'GET',
headers: {
                'Content-Type': 'application/json'
            , 
	Authorization: `Bearer ${token}`
		}
		
	}).then(res => res.json())
	  .then(data => {
	  	
	  	let courseData = data.reverse().map(courseData => {
	  		
	  		return (`
<section class="ctn card col-sm-3 m-3 hvr-curl-top-left hvr-bounce-in">
    <span class="card-body">
        <h1 class="text-center"><i class="fas fa-book-open" style="color:#f340cf"></i></h1>
        <h5 class="card-title text-center"> ${courseData.name} </h5>
        <p class="card-title text-center">
            ${courseData.description}</p>
        <p class="card-title text-center">
            &#8369;${courseData.price}</p>
    </span>
</section>
					`)
	  	
	  	}).join('')

	  	courseCardContainer.innerHTML= `${courseData}`


	 //  	<!-- `<div class="com-md-12">
		// 		<section class="jumbotron my-5" style="font-size: 60"px; display: flex;>

		// 			<h3 class="text-center"><br><br><b>Courses</b></h3>
		// 			<table class="table text-large">
						
		// 				<thead>
						
		// 					<tr style="font-size: 50" class="text-large">
		// 						<th> Name </th>
		// 						<th> Description </th>
		// 						<th> Price </th>
		// 					</tr>
						
		// 					<tbody>
								
								
								
		// 					</tbody>
		// 				</thead>
						
		// 			</table>
		// 		</section>
		// 	</div>
		// ` -->
	  })	
