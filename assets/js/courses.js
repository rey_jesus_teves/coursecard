let token = localStorage.getItem('token');

let courseContainer = document.querySelector('#courseContainer');


	fetch('https://rocky-harbor-60941.herokuapp.com/api/courses',{
		method: 'GET',
headers: {
                'Content-Type': 'application/json'
            , 
	Authorization: `Bearer ${token}`
		}
		
	}).then(res => res.json())
	  .then(data => {
	  	
	  	let courseData = data.reverse().map(courseData => {
	  		
	  		return(
	  			`
				<tr style="font-size: 60">
				   <td>${courseData.name}</td> 
				   <td>${courseData.description}</td> 
				   <td>₱${courseData.price}</td> 
				</tr> 
				`)
	  	}).join('')


	  	courseContainer.innerHTML= 


	  	`<div class="com-md-12">
				<section class="jumbotron my-2" style="font-size: 60"px; display: flex;>

					<h2 class="text-center mb-5"><b>Courses</b></h2>
					<table class="table text-large">
						<thead>
							<tr style="font-size: 50" class="text-large mb-5">
								<th> Name </th>
								<th> Description </th>
								<th> Price </th>
							</tr>
							<tbody>
								${courseData}
							</tbody>
						</thead>
					</table>
				</section>
			</div>
		`
	  })	
